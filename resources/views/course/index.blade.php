@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Manage Courses</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}

                            
                        </div>
                    @endif
                        @forelse ($courses as $course)
                            <p>
                               <h5> {{ $course->coursename}} </h5>
                            </p>
                        @empty
                            <p>No courses entered yet!</p>
                        @endforelse
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
